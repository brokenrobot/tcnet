﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonData.TornCity.Api {
    public class TornUserProfileLife {
        public int Current { get; set; }
        public int Maximum { get; set; }
        public int Increment { get; set; }
        public int Interval { get; set; }
        public int TickTime { get; set; }
        public int FullTime { get; set; }
    }
}
