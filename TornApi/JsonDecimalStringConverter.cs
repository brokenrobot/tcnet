﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    // the api responses but decimal numebrs in quotation marks which isn't handled by the built-in deserialization.
    // this custom converter adds support for quoted decimal numbers.
    class JsonDecimalStringConverter : JsonConverter<decimal> {
        public override decimal Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
            if (reader.TokenType == JsonTokenType.String) {
                decimal result;
                if (decimal.TryParse(reader.GetString(), out result)) {
                    return result;
                }
                else {
                    throw new JsonException("Could not convert value to decimal.");
                }
            }
            return reader.GetDecimal();
        }

        public override void Write(Utf8JsonWriter writer, decimal value, JsonSerializerOptions options) => throw new NotImplementedException();
    }
}
