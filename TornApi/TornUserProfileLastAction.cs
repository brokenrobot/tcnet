﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornUserProfileLastAction {
        public int Timestamp { get; set; }

        [JsonPropertyName("relative")]
        public string Description { get; set; }
    }
}
