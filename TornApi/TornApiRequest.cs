﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonData.TornCity.Api {
    internal class TornApiRequest {
        public TornApiCategory Category { get; set; }

        public string EntityId { get; set; }

        public List<string> Selections { get; } = new List<string>();

        public TornApiRequest(TornApiCategory apiCategory) => Category = apiCategory;
    }
}
