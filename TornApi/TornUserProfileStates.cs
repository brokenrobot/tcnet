﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornUserProfileStates {
        [JsonPropertyName("hospital_timestamp")]
        public int HospitalTimestamp { get; set; }

        [JsonPropertyName("jail_timestamp")]
        public int JailTimestamp { get; set; }
    }
}
