﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    internal class TornUserNetworth : TornApiResponseData {
        [JsonPropertyName("networth")]
        public TornUserNetworthDetails Details { get; set; }
    }
}
