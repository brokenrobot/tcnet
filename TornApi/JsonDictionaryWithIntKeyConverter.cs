﻿using System;
using System.Collections.Generic;
using System.Resources;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    class JsonDictionaryWithIntKeyConverter<T> : JsonConverter<Dictionary<int, T>> {
        private enum ConverterState {
            ReadKey,
            ReadObject
        }

        private ConverterState converterState = ConverterState.ReadKey;
        private int currentKey;

        public override Dictionary<int, T> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
            var result = new Dictionary<int, T>();
            do {
                if (converterState == ConverterState.ReadKey && reader.TokenType == JsonTokenType.PropertyName) {
                    if (int.TryParse(reader.GetString(), out int tryKey)) {
                        currentKey = tryKey;
                    }
                    converterState = ConverterState.ReadObject;
                }
                else if (converterState == ConverterState.ReadObject && reader.TokenType == JsonTokenType.StartObject) {
                    result.Add(currentKey, JsonSerializer.Deserialize<T>(ref reader, options));
                    converterState = ConverterState.ReadKey;
                }
            } while (reader.Read() && reader.TokenType != JsonTokenType.EndObject);
            return result;
        }

        public override void Write(Utf8JsonWriter writer, Dictionary<int, T> value, JsonSerializerOptions options) => throw new NotImplementedException();
    }
}
