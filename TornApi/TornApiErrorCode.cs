﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonData.TornCity.Api {
    public enum TornApiErrorCode {
        FailedProcessingErrorCode = -1,
        Unknown = 0,
        KeyEmpty = 1,
        InvalidKey = 2,
        WrongType = 3,
        WrongFields = 4,
        TooManyRequests = 5,
        IncorrectId = 6,
        IncorrectIdEntityRelation = 7,
        IpBlocked = 8,
        ApiDisabled = 9,
        KeyOwnerFedded = 10,
        KeyChangeError = 11,
        KeyReadError = 12
    }
}
