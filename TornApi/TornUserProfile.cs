﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornUserProfile : TornUserBasic {
        public string Rank { get; set; }

        [JsonPropertyName("property")]
        public string PropertyName { get; set; }

        [JsonPropertyName("property_id")]
        public int PropertyId { get; set; }

        public string SignUp { get; set; }

        public int Awards { get; set; }

        public int Friends { get; set; }

        public int Enemies { get; set; }

        [JsonPropertyName("forum_posts")]
        public int ForumPosts { get; set; }

        public int Karma { get; set; }

        public string Role { get; set; }

        public int Donator { get; set; }

        public TornUserBar Life { get; set; }

        public TornUserProfileJobInfo Job { get; set; }

        public TornUserProfileFactionInfo Faction { get; set; }

        public TornUserProfileMarriageInfo Married { get; set; }

        public Dictionary<string, string> BasicIcons { get; set; }

        public TornUserProfileStates States { get; set; }

        public TornUserProfileLastAction LastAction { get; set; }
    }
}
