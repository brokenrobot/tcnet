﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    internal class TornMedalsResponseData : TornApiResponseData {
        [JsonConverter(typeof(JsonDictionaryWithIntKeyConverter<TornMedal>))]
        public Dictionary<int, TornMedal> Medals { get; set; }
    }
}
