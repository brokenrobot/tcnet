﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornUserBasicStatus {
        public string Description { get; set; }

        public string Details { get; set; }

        public string State { get; set; }

        public string Color { get; set; }

        public long Until { get; set; }
    }
}
