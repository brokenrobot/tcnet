﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornApiError {
        public int Code { get; set; }

        [JsonPropertyName("error")]
        public string Message { get; set; }
    }
}
