﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonData.TornCity.Api {
    public class TornStockBenefit {
        public int Requirement { get; set; }
        public string Description { get; set; }
    }
}
