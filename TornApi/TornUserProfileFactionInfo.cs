﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornUserProfileFactionInfo {
        public string Position { get; set; }

        [JsonPropertyName("faction_id")]
        public int FactionId { get; set; }

        [JsonPropertyName("days_in_faction")]
        public int DaysInFaction { get; set; }

        [JsonPropertyName("faction_name")]
        public string FactionName { get; set; }
    }
}
