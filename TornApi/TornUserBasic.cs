﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornUserBasic : TornApiResponseData {
        public int Level { get; set; }

        public string Gender { get; set; }

        [JsonPropertyName("player_id")]
        public long PlayerId { get; set; }

        public string Name { get; set; }

        public TornUserBasicStatus Status { get; set; }
    }
}