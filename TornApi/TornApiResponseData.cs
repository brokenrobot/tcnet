﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornApiResponseData {
        [JsonPropertyName("timestamp")]
        public long ServerTimestamp { get; set; }

        public TornApiError Error { get; set; }
    }
}
