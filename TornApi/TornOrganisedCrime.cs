﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornOrganisedCrime {
        public string Name { get; set; }

        [JsonPropertyName("members")]
        public int MembersNeeded { get; set; }

        [JsonPropertyName("time")]
        public int DurationHours { get; set; }

        [JsonPropertyName("min_cash")]
        public long MinimumCash { get; set; }

        [JsonPropertyName("max_cash")]
        public long MaximumCash { get; set; }

        [JsonPropertyName("min_respect")]
        public int MinimumRespect { get; set; }

        [JsonPropertyName("max_respect")]
        public int MaximumRespect { get; set; }
    }
}
