﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    internal class TornOrganisedCrimeResponseData : TornApiResponseData {
        [JsonConverter(typeof(JsonDictionaryWithIntKeyConverter<TornOrganisedCrime>))]
        public Dictionary<int, TornOrganisedCrime> OrganisedCrimes { get; set; }
    }
}
