﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EonData.TornCity.Api {
    internal class TornApiClient : IDisposable {
        public string ApiKey { get; private set; }

        private const string UserAgentProductName = "libTcNet";
        private readonly string UserAgentProductVersion = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();
        private const string ApiUrlBase = @"https://api.torn.com/";
        private readonly HttpClient http;

        private readonly Dictionary<TornApiCategory, string> catmap = new Dictionary<TornApiCategory, string>() {
                { TornApiCategory.User, "User" },
                { TornApiCategory.Company, "Company" },
                { TornApiCategory.Faction, "Faction" },
                { TornApiCategory.Market, "ItemMarket" },
                { TornApiCategory.Properties, "Properties" },
                { TornApiCategory.Torn, "Torn" }
            };

        public TornApiClient(string key) {
            ApiKey = key;
            http = new HttpClient();
            AddUserAgentInfo(UserAgentProductName, UserAgentProductVersion);
        }

        public void AddUserAgentInfo(string ProductName, string ProductVersion) => http.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(ProductName, ProductVersion));

        public async Task<TornApiResponse<T>> SendRequestAsync<T>(TornApiRequest request, CancellationToken cancellationToken) where T : TornApiResponseData {
            string requestUrl = GetRequestUrl(request);
            HttpResponseMessage response = await http.GetAsync(requestUrl, HttpCompletionOption.ResponseHeadersRead);
            return new TornApiResponse<T>(response);
        }

        private string GetRequestUrl(TornApiRequest request) {
            var url = new StringBuilder($"{ApiUrlBase}{catmap[request.Category]}");
            if (!string.IsNullOrEmpty(request.EntityId)) {
                url.Append($"/{request.EntityId}");
            }

            url.Append("?");

            if (request.Selections.Any()) {
                url.Append($"selections={string.Join(',', request.Selections)}&");
            }

            url.Append($"key={ApiKey}");

            return url.ToString();
        }

        public void Dispose() => http.Dispose();
    }
}
