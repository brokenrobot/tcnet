﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonData.TornCity.Api {
    public enum TornApiCategory {
        User,
        Properties,
        Faction,
        Company,
        Market,
        Torn
    }
}
