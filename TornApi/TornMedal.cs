﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornMedal {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Type { get; set; }

        public long Circulation { get; set; }

        public string Rarity { get; set; }
    }
}
