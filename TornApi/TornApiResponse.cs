﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace EonData.TornCity.Api {
    internal class TornApiResponse<T> : IDisposable where T : TornApiResponseData {
        public string ErrorMessage { get; private set; }

        private readonly HttpResponseMessage response;
        private readonly JsonSerializerOptions jsonOpts = new JsonSerializerOptions() {
            PropertyNameCaseInsensitive = true,
        };

        internal TornApiResponse(HttpResponseMessage apiResponse) {
            jsonOpts.Converters.Add(new JsonDecimalStringConverter());
            response = apiResponse;
        }
        public async Task<T> ReadDataAsync(CancellationToken cancellationToken) {
            if (response.IsSuccessStatusCode) {
                await using (Stream jsonStream = await response.Content.ReadAsStreamAsync()) {
                    var result = await JsonSerializer.DeserializeAsync<T>(jsonStream, jsonOpts, cancellationToken);
                    if (result.Error == null) {
                        return result;
                    }
                    else {
                        ErrorMessage = $"API request failed. The API returned error code {result.Error.Code}: {result.Error.Message}";
                        return null;
                    }
                }
            }
            else {
                ErrorMessage = $"HTTP request failed. The server returned HTTP status code {response.StatusCode}: {response.ReasonPhrase}";
                return null;
            }
        }

        public void Dispose() => response.Dispose();
    }
}
