﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace EonData.TornCity.Api {
    public sealed class TornApi : IDisposable {
        private readonly TornApiClient torn;

        public TornApi(string apiKey) {
            torn = new TornApiClient(apiKey);
        }

        public async Task<TornUserBasic>GetBasicUserInfoAsync(string playerId = null, CancellationToken cancellationToken = default) {
            var req = CreateRequest(TornApiCategory.User);
            if (!string.IsNullOrEmpty(playerId)) {
                req.EntityId = playerId;
            }
            req.Selections.Add("basic");
            using var resp = await torn.SendRequestAsync<TornUserBasic>(req, cancellationToken);
            return await resp.ReadDataAsync(cancellationToken);
        }

        public async Task<TornUserProfile>GetUserProfileAsync(string playerId = null, CancellationToken cancellationToken = default) {
            var req = CreateRequest(TornApiCategory.User);
            if (!string.IsNullOrEmpty(playerId)) {
                req.EntityId = playerId;
            }
            req.Selections.Add("profile");
            using var resp = await torn.SendRequestAsync<TornUserProfile>(req, cancellationToken);
            return await resp.ReadDataAsync(cancellationToken);
        }

        public async Task<List<string>>GetAllSelectionsAsync(TornApiCategory category, CancellationToken cancellationToken = default) {
            var req = CreateRequest(category);
            req.Selections.Add("lookup");
            using var resp = await torn.SendRequestAsync<TornApiSelections>(req, cancellationToken);
            var respData = await resp.ReadDataAsync(cancellationToken);
            return respData?.Selections;
        }

        public async Task<TornUserNetworthDetails> GetUserNetworthAsync(string playerId = null, CancellationToken cancellationToken = default) {
            var req = CreateRequest(TornApiCategory.User);
            if (!string.IsNullOrEmpty(playerId)) {
                req.EntityId = playerId;
            }
            req.Selections.Add("networth");
            using var resp = await torn.SendRequestAsync<TornUserNetworth>(req, cancellationToken);
            var respData = await resp.ReadDataAsync(cancellationToken);
            return respData?.Details;
        }

        public async Task<TornUserBars>GetUserBarsAsync(string playerId = null, CancellationToken cancellationToken = default) {
            var req = CreateRequest(TornApiCategory.User);
            if (!string.IsNullOrEmpty(playerId)) {
                req.EntityId = playerId;
            }
            req.Selections.Add("bars");
            using var resp = await torn.SendRequestAsync<TornUserBars>(req, cancellationToken);
            return await resp.ReadDataAsync(cancellationToken);
        }

        public async Task<IDictionary<string, decimal>> GetGameStatsAsync(CancellationToken cancellationToken = default) { 
            var req = CreateRequest(TornApiCategory.Torn);
            req.Selections.Add("stats");
            using var resp = await torn.SendRequestAsync<TornGameStats>(req, cancellationToken);
            var result = await resp.ReadDataAsync(cancellationToken);
            return result.Stats;
        }

        public async Task<Dictionary<int, TornStock>> GetStocksAsync(CancellationToken cancellationToken = default) {
            var req = CreateRequest(TornApiCategory.Torn);
            req.Selections.Add("stocks");
            using var resp = await torn.SendRequestAsync<TornStocksResponseData>(req, cancellationToken);
            var respData = await resp.ReadDataAsync(cancellationToken);
            return respData.Stocks;
        }

        public async Task<Dictionary<int, TornItem>> GetItemsAsync(CancellationToken cancellationToken = default) {
            var req = CreateRequest(TornApiCategory.Torn);
            req.Selections.Add("items");
            using var resp = await torn.SendRequestAsync<TornItemsResponseData>(req, cancellationToken);
            var respData = await resp.ReadDataAsync(cancellationToken);
            return respData.Items;
        }

        public async Task<Dictionary<int, TornMedal>> GetMedalsAsync(CancellationToken cancellationToken = default) {
            var req = CreateRequest(TornApiCategory.Torn);
            req.Selections.Add("medals");
            using var resp = await torn.SendRequestAsync<TornMedalsResponseData>(req, cancellationToken);
            var respData = await resp.ReadDataAsync(cancellationToken);
            return respData.Medals;
        }

        public async Task<Dictionary<int, TornHonor>> GetHonorsAsync(CancellationToken cancellationToken = default) {
            var req = CreateRequest(TornApiCategory.Torn);
            req.Selections.Add("honors");
            using var resp = await torn.SendRequestAsync<TornHonorResponseData>(req, cancellationToken);
            var respData = await resp.ReadDataAsync(cancellationToken);
            return respData.Honors;
        }

        public async Task<Dictionary<int, TornOrganisedCrime>> GetOrganisedCrimesAsync(CancellationToken cancellationToken = default) {
            var req = CreateRequest(TornApiCategory.Torn);
            req.Selections.Add("organisedcrimes");
            using var resp = await torn.SendRequestAsync<TornOrganisedCrimeResponseData>(req, cancellationToken);
            var respData = await resp.ReadDataAsync(cancellationToken);
            return respData.OrganisedCrimes;
        }

        public async Task<Dictionary<int, TornGym>> GetGymsAsync(CancellationToken cancellationToken = default) {
            var req = CreateRequest(TornApiCategory.Torn);
            req.Selections.Add("gyms");
            using var resp = await torn.SendRequestAsync<TornGymsResponseData>(req, cancellationToken);
            var respData = await resp.ReadDataAsync(cancellationToken);
            return respData.Gyms;
        }

        public void Dispose() => torn.Dispose();

        private TornApiRequest CreateRequest(TornApiCategory category) {
            var r = new TornApiRequest(category);
            r.Selections.Add("timestamp");
            return r;
        }
    }
}