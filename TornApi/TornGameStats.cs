﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornGameStats : TornApiResponseData {
        public Dictionary<string, decimal> Stats { get; set; }
    }
}
