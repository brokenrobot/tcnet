﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornUserProfileJobInfo {
        public string Position { get; set; }

        [JsonPropertyName("company_id")]
        public int CompanyId { get; set; }

        [JsonPropertyName("company_name")]
        public string CompanyName { get; set; }
    }
}
