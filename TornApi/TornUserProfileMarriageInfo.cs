﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornUserProfileMarriageInfo {
        [JsonPropertyName("spouse_id")]
        public int SpousePlayerId { get; set; }

        [JsonPropertyName("spouse_name")]
        public string SpouseName { get; set; }

        public int Duration { get; set; }
    }
}
