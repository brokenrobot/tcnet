﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    internal class TornGymsResponseData : TornApiResponseData {
        [JsonConverter(typeof(JsonDictionaryWithIntKeyConverter<TornGym>))]
        public Dictionary<int, TornGym> Gyms { get; set; }
    }
}
