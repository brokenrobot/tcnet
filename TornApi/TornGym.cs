﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornGym {
        public string Name { get; set; }

        public int Stage { get; set; }

        public int Cost { get; set; }

        public int Energy { get; set; }
        
        public decimal Strength { get; set; }

        public decimal Speed { get; set; }

        public decimal Defense { get; set; }

        public decimal Dexterity { get; set; }

        public string Note { get; set; }
    }
}
