﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonData.TornCity.Api {
    public class TornUserChainBar {
        public int Current { get; set; }
        public int Maximum { get; set; }
        public int Timeout { get; set; }
        public float Modifier { get; set; }
        public int Cooldown { get; set; }
    }
}
