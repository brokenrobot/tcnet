﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornStock {

        public string Name { get; set; }

        public string Acronym { get; set; }

        public string Director { get; set; }

        [JsonPropertyName("current_price")]
        public decimal CurrentPrice { get; set; }

        [JsonPropertyName("market_cap")]
        public long MarketCap { get; set; }

        [JsonPropertyName("total_shares")]
        public long TotalShares { get; set; }

        [JsonPropertyName("available_shares")]
        public long AvailableShares { get; set; }

        public string Forecast { get; set; }

        public string Demand { get; set; }

        public TornStockBenefit Benefit { get; set; }
    }
}
