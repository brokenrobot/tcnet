﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornUserBars : TornApiResponseData {
        [JsonPropertyName("server_time")]
        public long Timestamp { get; set; }

        public TornUserBar Happy { get; set; }

        public TornUserBar Life { get; set; }

        public TornUserBar Energy { get; set; }

        public TornUserBar Nerve { get; set; }

        public TornUserChainBar Chain { get; set; }
    }
}
