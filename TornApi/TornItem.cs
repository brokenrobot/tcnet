﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    public class TornItem {
        public string Name { get; set; }

        public string Description { get; set; }

        [JsonPropertyName("type")]
        public string Category { get; set; }

        [JsonPropertyName("weapon_type")]
        public string WeaponType { get; set; }

        [JsonPropertyName("buy_price")]
        public long BuyPrice { get; set; }

        [JsonPropertyName("sell_price")]
        public long SellPrice { get; set; }

        [JsonPropertyName("market_value")]
        public long MarketValue { get; set; }

        public long Circulation { get; set; }

        public string Effect { get; set; }

        public string Requirement { get; set; }

        [JsonPropertyName("image")]
        public string ImageUrl { get; set; }
    }
}
