﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    internal class TornApiSelections : TornApiResponseData {
        public List<string> Selections { get; set; }
    }
}
