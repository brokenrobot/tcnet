﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonData.TornCity.Api {
    public class TornUserNetworthDetails : TornApiResponseData {
        public long Pending { get; set; }
        public long Wallet { get; set; }
        public long Bank { get; set; }
        public long Points { get; set; }
        public long Cayman { get; set; }
        public long Vault { get; set; }
        public long PiggyBank { get; set; }
        public long Items { get; set; }
        public long DisplayCase { get; set; }
        public long Bazaar { get; set; }
        public long Properties { get; set; }
        public long StockMarket { get; set; }
        public long AuctionHouse { get; set; }
        public long Company { get; set; }
        public long Bookie { get; set; }
        public long Loan { get; set; }
        public long UnpaidFees { get; set; }
        public long Total { get; set; }
    }
}
