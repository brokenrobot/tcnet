﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    internal class TornHonorResponseData : TornApiResponseData {
        [JsonConverter(typeof(JsonDictionaryWithIntKeyConverter<TornHonor>))]
        public Dictionary<int, TornHonor> Honors { get; set; }
    }
}
