﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EonData.TornCity.Api {
    internal class TornStocksResponseData : TornApiResponseData {
        [JsonConverter(typeof(JsonDictionaryWithIntKeyConverter<TornStock>))]
        public Dictionary<int, TornStock> Stocks { get; set; }
    }
}
