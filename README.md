# TC.Net 

TC.Net is a .NET library for accessing the web API of the online game Torn City.

## Installation

Use nuget to install TC.Net in your project.
It is a .NET Standard 2.1 library so it requires .NET Core 3.1 or above.

## Usage

```csharp
var api = new TornApi(apikey);
// add more stuff here once there is more to add...
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)