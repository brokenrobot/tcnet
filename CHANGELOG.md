# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.2.5] - 202-11-12
### Changed
- ***(breaking)*** Updated the target framework from .NET Standard 2.1 to .NET 5.0 to ensure that all the new platform features are available in the future. All consumer projects should be updated to target .NET 5

## [0.1.2.1] - 202-05-05
### Changed
- ***(breaking)*** Changed the return type of TornApi.GetGameStatsAsync() to Dictionary<string, decimal> instead of returning the TornApiResponseData derived type which aligns with the way the rest of the methods return their results.

## [0.1.2.0] - 202-04-26
### Added
- Added ability to read new data entities from the api: global game stats, items, stocks, medals, honors, as well as details about the different types of organised crimes and gyms.
### Changed
- ***(breaking)*** Refactored the TornApiResponse<T>.Data property into the TornApiResponse<T>.ReadDataAsync() method.

## [0.1.1.4] - 2020-04-13
### Added
- Added an optional CancellationToken parameter to all public data request methods on the TornApi class.
- Added a required CancellationToken parameter to the internal TornApiClient.SendRequestAsync() method. Having it required will help as a reminder that all public methods should support CancellationToken.

## [0.1.1.3] - 2020-02-24
### Fixed
- GetPlayerNetworthAsync() used the wrong selection value and always returned null.

## [0.1.1.2] - 2020-02-24
### Fixed
- Inheritance for TornUserNetworthDetails was incorrect and it now correctly inherits from TornApiResponseData.

## [0.1.1.1] - 2020-02-24
### Added
- All response data objects will now include the server timestamp.

## [0.1.1.0] - 2020-02-23
### Fixed
- Inheritance for TornUserProfile was incorrect and it now correctly inherits from TornUserBasic.
### Added
- UserAgent version is now read from the assembly information.

## [0.1.0.0] - 2020-02-22
### Added
- Low level client for communicating with the Torn web API.
- A mid-level client that handles errors and simplifies data retrieval.
- Classes necessary to get the following data entities: basic user info, user profile info, user timer bars, user networth details.